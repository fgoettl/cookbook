import {useEffect, useState} from "react";

export function FileInputOutput() {
    const [file, setFile] = useState<File | null>(null);
    const [text, setText] = useState<string>("");

    useEffect(() => {
        async function updateText(){
            if(file !== null) setText(await file.text());
        }
        updateText().catch(console.error);
    }, [file])

    function downloadText(text: string, fileName: string) {
        const element = document.createElement("a");
        const file = new Blob([text], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = `${fileName}_modified.txt`;
        document.body.appendChild(element);
        element.click();
    }

    return (
        <div>
            <input type="file" onChange={(e) => setFile(e.target.files !== null ? e.target.files[0] : null)}/>
            {file && (
                <section>
                    <div onClick={() => downloadText(text.toUpperCase(), file.name.replace(/\.[^/.]+$/, ""))} style={{backgroundColor:"red"}}> Download here </div>
                </section>
            )}
        </div>
    )
}
