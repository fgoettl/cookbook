import {Button, Center, Flex, Input, Select, Textarea} from "@chakra-ui/react";
import {useImmer} from "use-immer";

enum PlacementPreference {
    Earliest,
    Latest,
}

export interface RecipeData {
    text: string,
    durationMinutes: number,
    predecessor?: string,
    placement: PlacementPreference,
}

interface RecipeStepInputProps {
    allStepMarkers: string[],
    onSubmit: (recipe: RecipeData) => void,
}

export function RecipeStepInput({allStepMarkers, onSubmit}: RecipeStepInputProps) {
    const [recipeData, updateRecipeData] = useImmer<RecipeData>({
        text: "",
        durationMinutes: 0,
        predecessor: undefined,
        placement: PlacementPreference.Earliest,
    });

    function updateText(text: string) {
        updateRecipeData((draft) => {
            draft.text = text;
        });
    }

    function updatePredecessor(predecessor: string) {
        updateRecipeData((draft) => {
            draft.predecessor = predecessor;
        })
    }

    function updateDuration(durationDays: number) {
        updateRecipeData((draft) => {
            draft.durationMinutes = durationDays;
        })
    }

    return (
        <Center>
            <Flex direction="row"
                  overflow="hidden"
                  justifyContent="space-around"
                  alignItems="center"
                  bg="midnightblue"
                  borderRadius="10px"
                  p="10px">
                <div>
                    <Textarea bg="lightgrey"
                              width="40vw"
                              value={recipeData.text}
                              onChange={(e) => updateText(e.target.value)}/>
                </div>
                <Flex direction="column">
                    <div>
                        <Select bg="lightgrey"
                                value={recipeData.predecessor ?? ""}
                                onChange={(e) => updatePredecessor(e.target.value)}>
                            <option key={-1} value={""}>No predecessor</option>
                            {allStepMarkers.map((item, i) => (
                                <option key={i} value={item}>{item}</option>
                            ))}
                        </Select>
                    </div>
                    <div>
                        <Input type="number"
                               step="1"
                               bg="lightgrey"
                               value={recipeData.durationMinutes}
                               onChange={(e) => updateDuration(Number(e.target.value))}/>
                    </div>
                    <div>
                        <Select bg="lightgrey">
                            <option value="1">Item 1</option>
                        </Select>
                    </div>
                </Flex>
                <Button onClick={() => onSubmit(recipeData)}>Submit</Button>
            </Flex>
        </Center>
    )
}