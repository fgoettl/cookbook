import { GanttStyle } from "../../models/gantt-diagram/gantt-style";

export const DEFAULT_GANTT_STYLE: GanttStyle = {
    timeLinesProtrusionTop: 15,
    timeLinesProtrusionBottom: 5,
    timeLinesSpacing: (duration) => duration < 90 ? 15 : 30,
    timeLineThickness: 1,
    timeLineColor: "#575E54",
    timeStampBottomProtrusion: 12,
    timeStampColor: "#1F211E",
    timeCurrentColor: "#F00",
    timeCurrentThickness: 2,

    sectionColorGenerator: (index, ) => index % 2 == 0 ? "#B2E09B" : "#FFE", //: "#66A148",
    sectionHeadingLeftPadding: 5,
    sectionHeadingRightPadding: 5,
    sectionHeadingColor: "#1F211E",

    taskHeight: 20,
    taskBackgroundColor: "#97E072",
    taskBorderColor: "#27610B",
    taskBorderThickness: 2,
    taskBorderRadius: 5,
    taskBoxYMargin: 2,
    taskBoxXMargin: 2,
    taskTextColor: "#1F211E",

    bottomPadding: 20,
}