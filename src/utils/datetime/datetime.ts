export function toFilePostfix(date: Date): string {
    return date.toISOString().replace(/[-:.TZ]/g, '');
}