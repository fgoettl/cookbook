import {Instruction} from "../../models/recipes/instruction.ts";
import {Recipe} from "../../models/recipes/recipe.ts";
import {LATEX} from "./latex-constants.ts";

function instructionToLatex<DataType>(instruction: Instruction<DataType, object>): string {
    return instruction.specs.toLatex(instruction.data);
}

function recipeToLatex(recipe: Recipe): string {
    return (
        LATEX.RECIPE.START.BEFORE_TITLE +
        recipe.title +
        LATEX.RECIPE.START.AFTER_TITLE +
        recipe.instructions.map(instructionToLatex).join(LATEX.HRULE) +
        LATEX.RECIPE.END
    )
}

export function recipesToLatex(recipes: Recipe[]): string {
    return (
        LATEX.MAIN.START +
        recipes.map(recipeToLatex).join("\n") +
        LATEX.MAIN.END
    )
}