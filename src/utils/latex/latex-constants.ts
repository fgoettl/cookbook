export const LATEX = {
    MAIN: {
        START: `\\documentclass[a4]{book}
\\usepackage[most]{tcolorbox}
\\usepackage{varwidth}
\\usepackage[margin=2cm]{geometry}
\\usepackage{multicol}
\\pagestyle{plain}

\\newtcolorbox{recipe}[2][]{enhanced,
\tbefore skip=2mm,after skip=2mm,
\tcolback=black!5,colframe=black!50,boxrule=0.2mm,
\tattach boxed title to top left={xshift=1cm,yshift*=1mm-\\tcboxedtitleheight},
\tvarwidth boxed title*=-3cm,
\tboxed title style={frame code={
\t\t\t\\path[fill=tcbcolback!30!black]
\t\t\t([yshift=-1mm,xshift=-1mm]frame.north west)
\t\t\tarc[start angle=0,end angle=180,radius=1mm]
\t\t\t([yshift=-1mm,xshift=1mm]frame.north east)
\t\t\tarc[start angle=180,end angle=0,radius=1mm];
\t\t\t\\path[left color=tcbcolback!60!black,right color=tcbcolback!60!black,
\t\t\tmiddle color=tcbcolback!80!black]
\t\t\t([xshift=-2mm]frame.north west) -- ([xshift=2mm]frame.north east)
\t\t\t[rounded corners=1mm]-- ([xshift=1mm,yshift=-1mm]frame.north east)
\t\t\t-- (frame.south east) -- (frame.south west)
\t\t\t-- ([xshift=-1mm,yshift=-1mm]frame.north west)
\t\t\t[sharp corners]-- cycle;
\t\t},interior engine=empty,
\t},
\tfonttitle=\\bfseries,
\ttitle=#2,#1}

\\newcommand{\\einspaltig}[1]{\\begin{minipage}{\\textwidth}#1\\end{minipage}}
\\newcommand{\\strich}{\\vspace{.3em}\\hrule\\vspace{.3em}}
\\newcommand{\\zweispaltig}[2]{\\begin{minipage}{15em}#1\\end{minipage}\\hspace{.2em}\\vrule\\hspace{.2em}\\begin{minipage}{25em}#2\\end{minipage}}
\\newcommand{\\zutaten}[1]{\\begin{multicols}{3}#1\\end{multicols}}


\\begin{document}`,
        END: `\\end{document}`,
    },
    RECIPE: {
        START: {
            BEFORE_TITLE: `\\begin{recipe}[colbacktitle=green]{`,
            AFTER_TITLE: `}`,
        },
        END: `\\end{recipe}`,
    },
    HRULE: `\\vspace{.3em}\\hrule\\vspace{.2em}`
}
