import {GanttSection} from "../models/gantt-diagram/gantt-section.ts";
import {GanttTask} from "../models/gantt-diagram/gantt-task.ts";
import {clone} from "lodash";

interface GanttTimes {
    earliestRelevantTime: number,
    latestRelevantTime: number,
    timeSpan: number,
}

export function ganttTimes(tasks: GanttTask[]): GanttTimes {
    const earliestRelevantTime = tasks.map((task) => task.start).reduce((a, b) => Math.min(a, b), Infinity);
    const latestRelevantTime = tasks.map((task) => task.start + task.durationInMinutes).reduce((a, b) => Math.max(a, b), -Infinity);
    const timeSpan = latestRelevantTime - earliestRelevantTime;

    return {
        earliestRelevantTime: earliestRelevantTime,
        latestRelevantTime: latestRelevantTime,
        timeSpan: timeSpan,
    }
}

interface GanttSectionLengths {
    sectionsCount: number,
    lengths: number[],
    lengthPrefixSums: number[],
}

export function ganttSectionLengths(sections: GanttSection[], tasks: GanttTask[]): GanttSectionLengths {
    const sectionsCount = sections.length;
    const lengths = sections.map((section) => tasks.filter((t) => t.sectionId == section.id).length);
    const lengthPrefixSums: number[] = [];
    let prefixSum = 0;
    for (const length of lengths) {
        prefixSum += length;
        lengthPrefixSums.push(prefixSum);
    }
    return {
        sectionsCount: sectionsCount,
        lengths: lengths,
        lengthPrefixSums: lengthPrefixSums,
    }
}

export function sortedTasks(tasks: GanttTask[], sections: GanttSection[]): GanttTask[] {
    const sortedTasks = clone(tasks);
    sortedTasks.sort((t1, t2) => {return sections.findIndex((s) => s.id == t1.sectionId) - sections.findIndex((s) => s.id == t2.sectionId)});
    return sortedTasks;
}