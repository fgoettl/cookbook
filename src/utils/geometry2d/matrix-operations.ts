import {Mat2d} from "../../models/geometry2d/mat2d.ts";
import {Vec2d} from "../../models/geometry2d/vec2d.ts";

export function mulMV(mat: Mat2d, vec: Vec2d): Vec2d {
    return {x: mat.xx * vec.x + mat.xy * vec.y, y: mat.yx * vec.x + mat.yy * vec.y};
}

export function mulMM(m1: Mat2d, m2: Mat2d): Mat2d {
    return {
        xx: m1.xx * m2.xx + m1.xy * m2.yx,
        xy: m1.xx * m2.xy + m1.xy * m2.yy,
        yx: m1.yx * m2.xx + m1.yy * m2.yx,
        yy: m1.yx * m2.xy + m1.yy * m2.yy,
    }
}

export function mulSM(s: number, m: Mat2d): Mat2d {
    return {
        xx: s * m.xx,
        xy: s * m.xy,
        yx: s * m.yx,
        yy: s * m.yy,
    }
}

export function det(m: Mat2d): number {
    return m.xx * m.yy - m.xy * m.yx;
}

export function inv(m: Mat2d): Mat2d {
    const d = det(m);
    return {
        xx: m.yy / d,
        xy: -m.xy / d,
        yx: -m.yx / d,
        yy: m.xx / d,
    }
}