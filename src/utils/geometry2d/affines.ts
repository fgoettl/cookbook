import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {Affine2d} from "../../models/geometry2d/affine2d.ts";
import {IDENTITY, rotationMatrix} from "./matrices.ts";
import {mulMV} from "./matrix-operations.ts";

import {add, mulSV} from "./vector-operations.ts";

export function rotationTransformation(angle:number, origin: Vec2d = {x:0, y:0}): Affine2d{
    const mat = rotationMatrix(angle);
    const offset = add(origin, mulMV(mat, mulSV(-1, origin)))
    return {mat: mat, offset: offset};
}

export function translationTransformation(v: Vec2d): Affine2d{
    return {mat: IDENTITY, offset: v};
}