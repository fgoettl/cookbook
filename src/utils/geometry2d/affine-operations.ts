import {Affine2d} from "../../models/geometry2d/affine2d.ts";
import {mulMM, mulMV} from "./matrix-operations.ts";
import {Mat2d} from "../../models/geometry2d/mat2d.ts";
import {add} from "./vector-operations.ts";

export function mulAA(a1: Affine2d, a2: Affine2d): Affine2d {
    return {mat: mulMM(a1.mat, a2.mat), offset: add(mulMV(a1.mat, a2.offset), a1.offset)};
}

export function mulAM(a: Affine2d, m: Mat2d): Affine2d {
    return {mat: mulMM(a.mat, m), offset: a.offset};
}

export function mulMA(m: Mat2d, a: Affine2d): Affine2d {
    return {mat: mulMM(m, a.mat), offset: mulMV(m, a.offset)};
}