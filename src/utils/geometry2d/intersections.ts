import {Line} from "../../models/geometry2d/line.ts";
import {Circle} from "../../models/geometry2d/circle.ts";
import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {polarToCartesian} from "./polar-coordinates.ts";
import {add, mulSV, sp, sub, turnPiHalfs} from "./vector-operations.ts";

export function circleLineIntersection(line: Line, c: Circle): Vec2d[] {
    const EPS = 0.0000000001;

    const parallelUnit = polarToCartesian({r: 1, angle: line.angle});
    const orthogonalUnit = turnPiHalfs(parallelUnit);
    const orthogonalDelta = sp(orthogonalUnit, sub(line.anchor, c.center));
    const circleCenterLineDistance = Math.abs(orthogonalDelta);

    if(circleCenterLineDistance > c.radius + EPS) return [];
    if(circleCenterLineDistance > c.radius - EPS) return [add(c.center, mulSV(orthogonalDelta, orthogonalUnit))];

    const parallelOffset = Math.sqrt(c.radius*c.radius - orthogonalDelta*orthogonalDelta);
    const intersection1 = add(c.center, add(mulSV(orthogonalDelta, orthogonalUnit), mulSV(parallelOffset, parallelUnit)));
    const intersection2 = add(c.center, add(mulSV(orthogonalDelta, orthogonalUnit), mulSV(-parallelOffset, parallelUnit)));
    return [intersection1, intersection2];
}