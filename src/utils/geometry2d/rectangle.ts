import {
    IncompleteRectangleDimensions,
    RectangleDimensions
} from "../../models/geometry2d/rectangle-dimensions.ts";

export function completeRectangleDimensions({
                                                x,
                                                y,
                                                x2,
                                                y2,
                                                xc,
                                                yc,
                                                width,
                                                height,
                                                topLeft,
                                                topRight,
                                                bottomLeft,
                                                bottomRight,
                                                leftCenter,
                                                rightCenter,
                                                topCenter,
                                                bottomCenter,
                                                center,
                                            }: IncompleteRectangleDimensions): RectangleDimensions | undefined {
    // helper variables:
    const xMin = x ?? topLeft?.x ?? bottomLeft?.x ?? leftCenter?.x;
    const xCenter = xc ?? topCenter?.x ?? bottomCenter?.x ?? center?.x;
    const xMax = x2 ?? topRight?.x ?? bottomRight?.x ?? rightCenter?.x;
    const xSpan = width;
    const yMin = y ?? bottomLeft?.y ?? bottomRight?.y ?? bottomCenter?.y;
    const yCenter = yc ?? leftCenter?.y ?? rightCenter?.y ?? center?.y;
    const yMax = y2 ?? topLeft?.y ?? topRight?.y ?? topCenter?.y;
    const ySpan = height;

    const rectWidth =
        xSpan !== undefined ? xSpan :
            xMin !== undefined ? (
                    xMax !== undefined ? xMax - xMin :
                        xCenter !== undefined ? 2 * (xCenter - xMin) :
                            undefined
                ) :
                xMax !== undefined && xCenter !== undefined ? 2 * (xMax - xCenter) :
                    undefined;

    const rectHeight =
        ySpan !== undefined ? ySpan :
            yMin !== undefined ? (
                    yMax !== undefined ? yMax - yMin :
                        yCenter !== undefined ? 2 * (yCenter - yMin) :
                            undefined
                ) :
                yMax !== undefined && yCenter !== undefined ? 2 * (yMax - yCenter) :
                    undefined;

    const rectX =
        xMin !== undefined ? xMin :
            width !== undefined ? (
                    xCenter !== undefined ? xCenter - width / 2 :
                        xMax !== undefined ? xMax - width :
                            undefined
                ) :
                xCenter !== undefined && xMax !== undefined ? 2 * xCenter - xMax :
                    undefined;

    const rectY =
        yMin !== undefined ? yMin :
            height !== undefined ? (
                    yCenter !== undefined ? yCenter - height / 2 :
                        yMax !== undefined ? yMax - height :
                            undefined
                ) :
                yCenter !== undefined && yMax !== undefined ? 2 * yCenter - yMax :
                    undefined;

    if (rectX === undefined || rectY === undefined || rectWidth === undefined || rectHeight === undefined)
        return undefined;

    return {
        x: rectX,
        y: rectY,
        x2: rectX + rectWidth,
        y2: rectY + rectHeight,
        xc: rectX + rectWidth / 2,
        yc: rectY + rectHeight / 2,
        width: rectWidth,
        height: rectHeight,
        topLeft: {x: rectX, y: rectY + rectHeight},
        topRight: {x: rectX + rectWidth, y: rectY + rectHeight},
        bottomLeft: {x: rectX, y: rectY},
        bottomRight: {x: rectX + rectWidth, y: rectY},
        leftCenter: {x: rectX, y: rectY + rectHeight / 2},
        rightCenter: {x: rectX + rectWidth, y: rectY + rectHeight / 2},
        topCenter: {x: rectX + rectWidth / 2, y: rectY + rectHeight},
        bottomCenter: {x: rectX + rectWidth / 2, y: rectY},
        center: {x: rectX + rectWidth / 2, y: rectY + rectHeight / 2},
    }
}