import {Mat2d} from "../../models/geometry2d/mat2d.ts";
import {Vec2d} from "../../models/geometry2d/vec2d.ts";

export function rotationMatrix(angle: number): Mat2d {
    return {xx: Math.cos(angle), xy: -Math.sin(angle), yx: Math.sin(angle), yy: Math.cos(angle)};
}

export const IDENTITY: Mat2d = {
    xx: 1,
    xy: 0,
    yx: 0,
    yy: 1,
}

export const MIRROR_X: Mat2d = {
    xx: -1,
    xy: 0,
    yx: 0,
    yy: 1,
}

export const MIRROR_Y: Mat2d = {
    xx: 1,
    xy: 0,
    yx: 0,
    yy: -1,
}

export function rowMatrix(v1: Vec2d, v2: Vec2d): Mat2d {
    return {
        xx: v1.x,
        xy: v1.y,
        yx: v2.x,
        yy: v2.y,
    }
}

export function colMatrix(v1: Vec2d, v2: Vec2d): Mat2d {
    return {
        xx: v1.x,
        xy: v2.x,
        yx: v1.y,
        yy: v2.y,
    }
}

export function diagMatrix(v: Vec2d): Mat2d {
    return {
        xx: v.x,
        xy: 0,
        yx: 0,
        yy: v.y,
    }
}