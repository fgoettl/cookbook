import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {Line} from "../../models/geometry2d/line.ts";
import {angle as a, directionUnit} from "./polar-coordinates.ts";
import {abs, add, mulSV, sub} from "./vector-operations.ts";
import {Ray} from "../../models/geometry2d/ray.ts";
import {LineSegment} from "../../models/geometry2d/line-segment.ts";

export function lineThrough(v1: Vec2d, v2: Vec2d): Line {
    return {anchor: v1, angle: a(sub(v2, v1))};
}

export function rayToLine(r: Ray): Line {
    return {anchor: r.start, angle: r.angle};
}

interface PartialLineSegment {
    start?: Vec2d,
    end?: Vec2d,
    center?: Vec2d,
    arbitraryPoint?: Vec2d,
    length?: number,
    angle?: number | "horizontal" | "vertical",
}

export function lineSegment({
                                start,
                                end,
                                center,
                                arbitraryPoint,
                                length,
                                angle,
                            }: PartialLineSegment): LineSegment | null {
    const _angle =
        angle !== undefined ? (
                                angle == "horizontal" ? 0 :
                                angle == "vertical" ? Math.PI :
                                angle
                            ) :
        start !== undefined ? (
                                end !== undefined ? a(sub(end, start)) :
                                center !== undefined ? a(sub(center, start)) :
                                arbitraryPoint !== undefined ? a(sub(arbitraryPoint, start)) :
                                undefined
                            ) :
        end !== undefined ? (
                              center !== undefined ? a(sub(end, center)) :
                              arbitraryPoint !== undefined ? a(sub(end, arbitraryPoint)) :
                              undefined
                          ) :
        center !== undefined ? (
                                 arbitraryPoint !== undefined ? a(sub(arbitraryPoint, center)) :
                                 undefined
                             ) :
        undefined;
    const _length =
        length !== undefined ? length :
        start !== undefined ? (
                                end !== undefined ? abs(sub(end, start)) :
                                center !== undefined ? 2 * abs(sub(center, start)) :
                                undefined
                            ) :
        end !== undefined ? (
                              center !== undefined ? 2 * abs(sub(end, center)) :
                              undefined
                          ) :
        undefined;

    if (_length === undefined || _angle === undefined) return null;

    const _unit = directionUnit(_angle);

    let x1 = undefined;
    let y1 = undefined;
    let x2 = undefined;
    let y2 = undefined;
    if (start !== undefined) {
        x1 = start.x;
        y1 = start.y;
        const e = add(start, mulSV(_length, _unit))
        x2 = e.x;
        y2 = e.y;
    } else if (end !== undefined) {
        x2 = end.x;
        y2 = end.y;
        const s = add(end, mulSV(-_length, _unit))
        x1 = s.x;
        y1 = s.y;
    } else if (center !== undefined) {
        const s = add(center, mulSV(-_length / 2, _unit))
        const e = add(center, mulSV(_length / 2, _unit))
        x1 = s.x;
        y1 = s.y;
        x2 = e.x;
        y2 = e.y;
    }
    return {start: {x: x1!, y: y1!}, end: {x: x2!, y: y2!}};
}