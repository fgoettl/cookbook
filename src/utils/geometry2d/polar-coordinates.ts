import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {PolarPoint} from "../../models/geometry2d/polar-point.ts";
import {abs} from "./vector-operations.ts";

export function angle(v: Vec2d): number {
    return v.x > 0 ? Math.atan(v.y / v.x) :
           v.x < 0 && v.y >= 0 ? Math.atan(v.y / v.x) + Math.PI :
           v.x < 0 ? Math.atan(v.y / v.x) - Math.PI :
           v.y > 0 ? Math.PI / 2 :
           v.y < 0 ? -Math.PI / 2 :
           0;
}

export function polarToCartesian(polarCoordinates: PolarPoint): Vec2d {
    return {
        x: polarCoordinates.r * Math.cos(polarCoordinates.angle),
        y: polarCoordinates.r * Math.sin(polarCoordinates.angle),
    }
}

export function cartesianToPolar(v: Vec2d): PolarPoint {
    return {
        r: abs(v),
        angle: angle(v),
    }
}

export function directionUnit(angle: number): Vec2d {
    return {x: Math.cos(angle), y: Math.sin(angle)};
}