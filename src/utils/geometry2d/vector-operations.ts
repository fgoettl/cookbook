import {Vec2d} from "../../models/geometry2d/vec2d.ts";

export function mulSV(factor: number, vec: Vec2d): Vec2d {
    return {x: factor * vec.x, y: factor * vec.y};
}

export function add(vec1: Vec2d, vec2: Vec2d): Vec2d {
    return {x: vec1.x + vec2.x, y: vec1.y + vec2.y};
}

export function sub(vec1: Vec2d, vec2: Vec2d): Vec2d {
    return {x: vec1.x - vec2.x, y: vec1.y - vec2.y};
}

export function abs(v: Vec2d): number {
    return Math.sqrt(v.x * v.x + v.y * v.y);
}

export function neg(v: Vec2d): Vec2d {
    return {x: -v.x, y: -v.y};
}

export function normalize(v: Vec2d): Vec2d{
    const length = abs(v);
    return length == 0 ? v : mulSV(1/length, v);
}

export function sp(v1: Vec2d, v2: Vec2d): number {
    return v1.x * v2.x + v1.y * v2.y;
}

export function turnPiHalfs(v: Vec2d): Vec2d{
    return {x: -v.y, y: v.x};
}
