export function zip<S, T>(a: Array<S>,b: Array<T>): Array<[S,T]> {
    return a.slice(0,b.length).map((ai, i) => [ai, b[i]]);
}