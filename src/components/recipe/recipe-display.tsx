import {useEffect, useState} from "react";
import {type Recipe} from "../../models/recipes/recipe.ts";
import {DraftFunction, Updater, useImmer} from "use-immer";
import {zip} from "../../utils/functional.ts";
import {isFunction} from "lodash";
import {Button, Paper, Typography} from "@mui/material";
import React from "react";
import {InstructionDivider} from "./instruction-divider.tsx";
import DeleteIcon from '@mui/icons-material/Delete';
import {Instruction} from "../../models/recipes/instruction.ts";

type RecipeProps = {
    recipe: Recipe,
    updateRecipe: Updater<Recipe>,
}

export function RecipeDisplay({recipe, updateRecipe}: RecipeProps) {
    const [mode, setMode] = useState<"Edit" | "Show">("Show");
    const [drafts, updateDrafts] = useImmer<object[]>(recipe.instructions.map(instruction => instruction.specs.madeDraft(instruction.data)));

    useEffect(() => {
        updateDrafts(recipe.instructions.map(instruction => instruction.specs.madeDraft(instruction.data)));
    }, [recipe.id])

    const instructionsAndDrafts = zip(recipe.instructions, drafts);

    function switchToEditMode() {
        updateDrafts(() => {
            return recipe.instructions.map(instruction => instruction.specs.madeDraft(instruction.data))
        });
        setMode("Edit");
    }

    function switchToShowMode() {
        if (instructionsAndDrafts.every(([instruction, draft]) => instruction.specs.draftSavable(draft))) {
            updateRecipe(recipe => {
                zip(recipe.instructions, drafts).forEach(([instruction, draft]) => {
                    instruction.data = instruction.specs.finalized(draft);
                })
            })
            setMode("Show");
        }
    }

    function switchMode() {
        if (mode === "Edit") switchToShowMode();
        else switchToEditMode();
    }

    function updateDraft(arg: object | DraftFunction<object>, index: number) {
        updateDrafts(drafts => {
            if (isFunction(arg)) arg(drafts[index]);
            else drafts[index] = arg;
        })
    }

    function deleteInstruction(index: number) {
        updateRecipe(recipe => {
            recipe.instructions = recipe.instructions.filter((_, i) => i !== index);
        })
        updateDrafts(() => {
            return drafts.filter((_, i) => i !== index);
        })
    }

    function addInstruction(index: number, instruction: Instruction<unknown, object>) {
        updateRecipe(recipe => {
            recipe.instructions = [...recipe.instructions.slice(0, index), instruction, ...recipe.instructions.slice(index)]
        })
        updateDrafts(() => {
            return [...drafts.slice(0, index), instruction.specs.madeDraft(instruction.data), ...drafts.slice(index)]
        })
    }

    return (
        <div className="max-w-5xl w-full flex flex-col">
            <div className="my-4 flex flex-row gap-8">
                <Typography variant="h4" component="span">
                    {recipe.title}
                </Typography>
                <Button variant="contained" onClick={switchMode}>{mode === "Edit" ? "Speichern" : "Bearbeiten"}</Button>
            </div>
            <InstructionDivider mode={mode} position={0} addInstruction={addInstruction}/>
            {instructionsAndDrafts.map(([instruction, draft], index) => (

                <React.Fragment key={instruction.id}>
                    <div className="flex flex-row w-full items-center">
                        <Paper elevation={12} className="mx-8 flex-grow">
                            {mode === "Show" ?
                             <instruction.specs.display mode={"Show"} data={instruction.data}/>
                                             :
                             <instruction.specs.display key={instruction.id} mode={"Edit"} draft={draft}
                                                        updateDraft={(arg) => updateDraft(arg, index)}/>
                            }
                        </Paper>
                        {mode === "Edit" &&
                            <div className="mr-8">
                                <Button variant="outlined" color="secondary" onClick={() => deleteInstruction(index)}>
                                    <DeleteIcon/>
                                </Button>
                            </div>
                        }
                    </div>
                    <InstructionDivider mode={mode} position={index + 1} addInstruction={addInstruction}/>
                </React.Fragment>
            ))}
        </div>
    )
}