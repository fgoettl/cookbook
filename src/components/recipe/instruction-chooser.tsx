import {Dialog, DialogTitle, List, ListItem, ListItemText, ListItemButton} from "@mui/material";
import {Instruction} from "../../models/recipes/instruction.ts";
import {generateInstruction, INSTRUCTION_SPECS} from "../instructions/generate-instruction.ts";

interface InstructionChooserProps {
    onChoose: (instruction: Instruction<unknown, object>) => void,
    onClose: () => void,
    open: boolean,
}

export function InstructionChooser({onChoose, onClose, open}: InstructionChooserProps) {
    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Vorlage wählen:</DialogTitle>
            <List sx={{pt: 0}}>
                {INSTRUCTION_SPECS.map((specs) => (
                    <ListItem disableGutters key={specs.title}>
                        <ListItemButton onClick={() => onChoose(generateInstruction(specs))}>
                            <ListItemText primary={specs.title} secondary={specs.description}/>
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Dialog>
    );
}