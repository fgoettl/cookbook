import {Button, Dialog, DialogActions, DialogTitle, TextField} from "@mui/material";
import {useState} from "react";
import {createRecipeSelector} from "../../store/recipe-selectors.ts";
import {useFeastableStore} from "../../store/store.ts";

interface InstructionChooserProps {
    onClose: () => void,
    open: boolean,
}

export function RecipeCreator({onClose, open}: InstructionChooserProps) {
    const [title, setTitle] = useState("");
    const createRecipe = useFeastableStore(createRecipeSelector);

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Neues Rezept</DialogTitle>
            <TextField id="filled-basic"
                       label="Titel"
                       variant="filled"
                       value={title}
                       onChange={e => setTitle(e.target.value)}/>

            <DialogActions>
                <Button autoFocus
                        onClick={() => {
                            createRecipe(title);
                            onClose();
                        }}>
                    Rezept erstellen
                </Button>
            </DialogActions>
        </Dialog>
    );
}