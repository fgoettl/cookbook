import {Button, Divider} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import {InstructionChooser} from "./instruction-chooser.tsx";
import {Instruction} from "../../models/recipes/instruction.ts";
import {useState} from "react";

interface InstructionDividerProps {
    mode: "Show" | "Edit",
    position: number,
    addInstruction: (index: number, instruction: Instruction<unknown, object>) => void,
}

export function InstructionDivider({mode, position, addInstruction}: InstructionDividerProps) {
    const [open, setOpen] = useState(false);
    return (
        <div className="my-4">
            <Divider variant="middle" component="div" flexItem>
                {mode === "Edit" &&
                    <Button variant="outlined" onClick={() => setOpen(true)}>
                        <AddIcon/>
                    </Button>
                }
                {mode === "Edit" &&
                    <InstructionChooser onChoose={(instruction) => {
                        addInstruction(position, instruction);
                        setOpen(false);
                    }}
                                        onClose={() => setOpen(false)}
                                        open={open}/>
                }
            </Divider>
        </div>
    )
}