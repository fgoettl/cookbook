import {Button, Dialog, DialogActions, DialogTitle, TextField} from "@mui/material";
import {useState} from "react";
import {exportStoreSelector} from "../../store/recipe-selectors.ts";
import {useFeastableStore} from "../../store/store.ts";
import {toFilePostfix} from "../../utils/datetime/datetime.ts";

interface ExporterProps {
    onClose: () => void,
    open: boolean,
}

export function Exporter({onClose, open}: ExporterProps) {
    const [fileName, setFileName] = useState(`my_feastable_${toFilePostfix(new Date(Date.now()))}`);
    const exportStore = useFeastableStore(exportStoreSelector);

    function exportDatabase() {
        const element = document.createElement("a");
        const file = new Blob([exportStore], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = fileName + ".feast";
        document.body.appendChild(element);
        element.click();
    }

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Exportieren als:</DialogTitle>
            <TextField className="w-80"
                       id="filled-basic"
                       label="Titel"
                       variant="filled"
                       value={fileName}
                       onChange={e => setFileName(e.target.value)}/>

            <DialogActions>
                <Button autoFocus
                        onClick={() => {
                            exportDatabase();
                            onClose();
                        }}>
                    exportieren
                </Button>
            </DialogActions>
        </Dialog>
    );
}