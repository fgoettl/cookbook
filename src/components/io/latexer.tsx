import {Button, Dialog, DialogActions, DialogTitle, TextField} from "@mui/material";
import {useState} from "react";
import {getAllRecipesSelector} from "../../store/recipe-selectors.ts";
import {useFeastableStore} from "../../store/store.ts";
import {toFilePostfix} from "../../utils/datetime/datetime.ts";
import {recipesToLatex} from "../../utils/latex/latex.ts";

interface LatexerProps {
    onClose: () => void,
    open: boolean,
}

export function Latexer({onClose, open}: LatexerProps) {
    const [fileName, setFileName] = useState(`my_feastable_${toFilePostfix(new Date(Date.now()))}`);
    const recipes = useFeastableStore(getAllRecipesSelector);

    function exportLatex() {
        const element = document.createElement("a");
        const file = new Blob([recipesToLatex(recipes)], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = fileName + ".tex";
        document.body.appendChild(element);
        element.click();
    }

    function exportScript() {
        const element = document.createElement("a");
        const file = new Blob(["pdflatex " + fileName + ".tex"], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "compile.bat";
        document.body.appendChild(element);
        element.click();
    }


    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Als LaTeX exportieren:</DialogTitle>
            <TextField className="w-80"
                       id="filled-basic"
                       label="Titel"
                       variant="filled"
                       value={fileName}
                       onChange={e => setFileName(e.target.value)}/>

            <DialogActions>
                <Button autoFocus
                        onClick={() => {
                            exportLatex();
                            onClose();
                        }}>
                    exportieren
                </Button>

                <Button autoFocus
                        onClick={() => {
                            exportScript();
                            onClose();
                        }}>
                    Compiler herunterladen
                </Button>
            </DialogActions>
        </Dialog>
    );
}