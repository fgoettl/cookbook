import {useEffect, useState} from "react";
import {useFeastableStore} from "../../store/store.ts";
import {importStoreSelector} from "../../store/recipe-selectors.ts";
import {Button, Dialog, DialogActions, DialogTitle} from "@mui/material";

interface ImporterProps {
    onClose: () => void,
    open: boolean,
}

export function Importer({onClose, open}: ImporterProps) {
    const [file, setFile] = useState<File | null>(null);
    const importStore = useFeastableStore(importStoreSelector);
    const [fileContent, setFileContent] = useState<string>("");

    useEffect(() => {
        async function updateText() {
            if (file !== null && file.name.split('.').pop() === "feast")
                setFileContent(await file.text());
        }
        updateText().catch(console.error);
    }, [file])

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Datei auswählen:</DialogTitle>
            <Button variant="contained" component="label">
                {file ? file.name : "Datei auswählen"}
                <input type="file"
                       hidden
                       onChange={(e) => setFile(e.target.files !== null ? e.target.files[0] : null)}/>
            </Button>

            <DialogActions>
                <Button autoFocus
                        onClick={() => {
                            importStore(fileContent);
                            onClose();
                        }}>
                    importieren
                </Button>
            </DialogActions>
        </Dialog>
    );
}