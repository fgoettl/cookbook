import {
    InstructionDisplayProps,
    InstructionEditProps,
    InstructionShowProps,
    InstructionSpecs
} from "../../models/recipes/instruction.ts";
import {TextField, Typography} from "@mui/material";

export function DisplaySimpleText(props: InstructionDisplayProps<string, { text: string }>) {
    return (
        props.mode == "Show" ? <ShowSimpleText {...props}/> : <EditSimpleText {...props}/>
    )
}

function ShowSimpleText({data}: InstructionShowProps<string>) {
    return (
        <Typography variant="body2" component="span">
            {data}
        </Typography>
    )
}

function EditSimpleText({draft, updateDraft}: InstructionEditProps<{ text: string }>) {
    return (
        <TextField
            className="w-full"
            color="secondary"
            id="filled-multiline-static"
            label="Bitte Text eintragen"
            multiline
            variant="filled"
            value={draft.text}
            onChange={(event) => {
                updateDraft(draft => {
                    draft.text = event.target.value
                });
            }}
        />
    )
}

export const simpleText: InstructionSpecs<string, { text: string }> = {
    id: 1,
    title: "Simple Text",
    description: "Simple, full width text.",
    defaultData: "Enter text.",
    toLatex: instruction => "H" + instruction + "H",
    draftSavable: () => true,
    finalized: draft => draft.text,
    madeDraft: data => ({text: data}),
    display: DisplaySimpleText,
}