import {Instruction, InstructionSpecs} from "../../models/recipes/instruction.ts";
import {simpleText} from "./simple-text.tsx";

export const INSTRUCTION_SPECS: InstructionSpecs<unknown, object>[] = [
    simpleText as InstructionSpecs<unknown, object>,
]

export function generateInstruction<DataType, DraftType extends object>(specs: InstructionSpecs<DataType, DraftType>): Instruction<DataType, DraftType> {
    return {
        id: Math.floor(Math.random() * 1000000000000),
        data: specs.defaultData,
        specs: specs,
    }
}