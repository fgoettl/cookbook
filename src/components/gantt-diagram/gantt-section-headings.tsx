import {GanttSection} from "../../models/gantt-diagram/gantt-section.ts";
import {ganttSectionLengths} from "../../utils/gantt.ts";
import {SvgText} from "../svg-base/svg-text.tsx";
import {useContext} from "react";
import {GanttStyleContext} from "./gantt-style-context.tsx";
import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";

interface GanttSectionHeadingsProps {
    sections: GanttSection[],
    tasks: GanttTask[],
    setWidth: (updater: (oldWidth: number) => number) => void,
}

export function GanttSectionHeadings({sections, tasks, setWidth}: GanttSectionHeadingsProps) {
    const style = useContext(GanttStyleContext);
    const {lengths, lengthPrefixSums, sectionsCount} = ganttSectionLengths(sections, tasks);
    return (
        sections.map((section, index) =>
            <g key={index}
               ref={(node) => {
                   if (node !== null) {
                       setWidth((oldWidth) => Math.max(oldWidth, node.getBBox().width));
                   }
               }}>
                <SvgText x={style.sectionHeadingLeftPadding}
                         y={style.bottomPadding + (lengthPrefixSums[sectionsCount - 1] - lengthPrefixSums[index] + lengths[index] / 2) * style.taskHeight}
                         textAnchor={"start"}
                         color={style.sectionHeadingColor}>
                    {section.title}
                </SvgText>
            </g>)
    )
}