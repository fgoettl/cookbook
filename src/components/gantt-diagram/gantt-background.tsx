import {GanttSection} from "../../models/gantt-diagram/gantt-section.ts";
import {range} from "lodash";
import {SvgRectangle} from "../svg-base/svg-rectangle.tsx";
import {GanttStyleContext} from "./gantt-style-context.tsx";
import {useContext} from "react";
import {ganttSectionLengths} from "../../utils/gantt.ts";
import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";

interface GanttBackgroundProps {
    width: number,
    sections: GanttSection[],
    tasks: GanttTask[],
}

export function GanttBackground({
                                    width,
                                    sections,
                                    tasks,
                                }: GanttBackgroundProps) {
    const style = useContext(GanttStyleContext);
    const {sectionsCount, lengths, lengthPrefixSums} = ganttSectionLengths(sections, tasks);
    return (
        <>
            {range(0, sections.length).map((index) =>
                <SvgRectangle key={index}
                              dimensions={{
                                  x: 0,
                                  y: style.bottomPadding + (lengthPrefixSums[sectionsCount - 1] - lengthPrefixSums[index]) * style.taskHeight,
                                  width: width,
                                  height: style.taskHeight * lengths[index],
                              }}
                              fillColor={style.sectionColorGenerator(index, sectionsCount)}/>)}
        </>
    )
}
