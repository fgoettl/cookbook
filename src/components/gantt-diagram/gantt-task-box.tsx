import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";
import {SvgRectangle} from "../svg-base/svg-rectangle.tsx";
import {useContext} from "react";
import {GanttStyleContext} from "./gantt-style-context.tsx";
import {SvgText} from "../svg-base/svg-text.tsx";

interface GanttTaskBoxProps {
    task: GanttTask,
    taskNumber: number,
    taskCount: number,
    xOfZeroMinutes: number,
    widthOfMinute: number,
    currentTime?: number,
}

export function GanttTaskBox({
                                 task,
                                 taskNumber,
                                 taskCount,
                                 xOfZeroMinutes,
                                 widthOfMinute,
                                 currentTime,
                             }: GanttTaskBoxProps) {
    const style = useContext(GanttStyleContext);
    const x = xOfZeroMinutes + widthOfMinute * task.start + style.taskBoxXMargin;
    const y = style.bottomPadding + (taskCount - 1 - taskNumber) * style.taskHeight + style.taskBoxYMargin;
    const width = widthOfMinute * task.durationInMinutes - 2 * style.taskBoxXMargin;
    const height = style.taskHeight - 2 * style.taskBoxYMargin;

    const taskActive = currentTime !== undefined && currentTime >= task.start && currentTime <= task.start + task.durationInMinutes;

    return (
        <>
            <SvgRectangle dimensions={{x: x, width: width, y: y, height: height}}
                          borderThickness={style.taskBorderThickness}
                          borderColor={taskActive ? "red" : style.taskBorderColor}
                          fillColor={style.taskBackgroundColor}
                          r={style.taskBorderRadius}/>
            <SvgText x={x + width / 2}
                     y={y + height / 2}
                     color={style.taskTextColor}
                     maxWidth={width}>
                {task.title}
            </SvgText>
        </>
    )
}