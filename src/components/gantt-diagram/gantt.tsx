import {GanttBackground} from "./gantt-background.tsx";
import {GanttStyleContext, GanttStyleContextProvider} from "./gantt-style-context.tsx";
import {DEFAULT_GANTT_STYLE} from "../../data/gantt/gantt-styles.ts";
import {GanttTaskBox} from "./gantt-task-box.tsx";
import {ganttTimes, sortedTasks} from "../../utils/gantt.ts";
import {GanttSectionHeadings} from "./gantt-section-headings.tsx";
import {useContext, useState} from "react";
import {GanttTimestamps} from "./gantt-timestamps.tsx";
import {Svg} from "../svg-base/svg.tsx";
import {GanttInstructions} from "../../models/gantt-diagram/gantt-instructions.ts";

interface GanttProps {
    ganttInstructions: GanttInstructions,
    width: number,
    currentTime?: number,
}

export function Gantt({ganttInstructions, width, currentTime}: GanttProps) {
    const style = useContext(GanttStyleContext);
    const [sectionHeadingWidth, setSectionHeadingWidth] = useState<number>(0);

    const sections = ganttInstructions.sections;
    const tasks = ganttInstructions.tasks;

    if (sections.length == 0 || tasks.length == 0) return null;

    const {earliestRelevantTime, timeSpan} = ganttTimes(tasks);
    const widthOfMinute = (width - (sectionHeadingWidth + style.sectionHeadingLeftPadding + style.sectionHeadingRightPadding)) / timeSpan;
    const xOfZeroMinutes = -earliestRelevantTime * widthOfMinute;
    return (
        <GanttStyleContextProvider style={DEFAULT_GANTT_STYLE}>
            <Svg width={width}
                 height={tasks.length * style.taskHeight + style.bottomPadding + style.timeLinesProtrusionTop}
                 backgroundColor={"transparent"}>
                <GanttBackground width={width}
                                 sections={sections} tasks={tasks}/>
                <GanttSectionHeadings sections={sections} tasks={tasks} setWidth={setSectionHeadingWidth}/>
                <g transform={`translate(${sectionHeadingWidth + style.sectionHeadingLeftPadding + style.sectionHeadingRightPadding} 0)`}>
                    <GanttTimestamps sections={sections} tasks={tasks} widthOfMinute={widthOfMinute} currentTime={currentTime}/>
                    {sortedTasks(tasks, sections).map((task, index) => (
                        <GanttTaskBox key={task.id} task={task} taskNumber={index} taskCount={tasks.length}
                                      xOfZeroMinutes={xOfZeroMinutes}
                                      widthOfMinute={widthOfMinute}
                                      currentTime={currentTime}/>
                    ))}
                </g>
            </Svg>
        </GanttStyleContextProvider>
    )
}