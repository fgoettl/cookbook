import {GanttStyle} from "../../models/gantt-diagram/gantt-style.tsx";
import {createContext, PropsWithChildren} from "react";
import {DEFAULT_GANTT_STYLE} from "../../data/gantt/gantt-styles.ts";

interface GanttStyleContextProviderProps extends PropsWithChildren{
    style: GanttStyle,
}

export const GanttStyleContext = createContext<GanttStyle>(DEFAULT_GANTT_STYLE);

export function GanttStyleContextProvider({style, children}: GanttStyleContextProviderProps) {
    return (
    <GanttStyleContext.Provider value={style}>
        {children}
    </GanttStyleContext.Provider>
    )
}