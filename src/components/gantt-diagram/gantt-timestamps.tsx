import {range} from "lodash";
import {Fragment, useContext} from "react";
import {SvgLineSegment} from "../svg-base/svg-line-segment.tsx";
import {SvgText} from "../svg-base/svg-text.tsx";
import {GanttStyleContext} from "./gantt-style-context.tsx";
import {ganttSectionLengths, ganttTimes} from "../../utils/gantt.ts";
import {GanttSection} from "../../models/gantt-diagram/gantt-section.ts";
import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";

interface GanttTimestampsProps {
    sections: GanttSection[],
    tasks: GanttTask[],
    widthOfMinute: number,
    currentTime?: number,
}

export function GanttTimestamps({sections, tasks, widthOfMinute, currentTime}: GanttTimestampsProps) {
    const style = useContext(GanttStyleContext);
    const {sectionsCount, lengthPrefixSums} = ganttSectionLengths(sections, tasks);
    const {timeSpan} = ganttTimes(tasks);
    const timeLineSpacing = style.timeLinesSpacing(timeSpan);
    const timeLineCount = Math.floor(timeSpan / timeLineSpacing) + 1;
    return (
        <>
            {range(0, timeLineCount).map((index) =>
                <Fragment key={sections.length + index}>
                    <SvgLineSegment
                        lineSegment={{
                            start: {
                                x: index * timeLineSpacing * widthOfMinute,
                                y: style.bottomPadding - style.timeLinesProtrusionBottom,
                            },
                            end: {
                                x: index * timeLineSpacing * widthOfMinute,
                                y: style.bottomPadding + style.timeLinesProtrusionTop + style.taskHeight * lengthPrefixSums[sectionsCount - 1],
                            },
                        }}

                        color={style.timeLineColor}
                        thickness={style.timeLineThickness}/>
                    <SvgText anchor={{
                        x: index * timeLineSpacing * widthOfMinute,
                        y: style.bottomPadding - style.timeStampBottomProtrusion,
                    }}
                             color={style.timeStampColor}>
                        {index * timeLineSpacing}
                    </SvgText>
                </Fragment>)
            }
            {currentTime !== undefined &&
                <SvgLineSegment
                    lineSegment={{
                        start: {
                            x: currentTime * widthOfMinute,
                            y: style.bottomPadding - style.timeLinesProtrusionBottom,
                        },
                        end: {
                            x: currentTime * widthOfMinute,
                            y: style.bottomPadding + style.timeLinesProtrusionTop + style.taskHeight * lengthPrefixSums[sectionsCount - 1],
                        },
                    }}
                    color={style.timeCurrentColor}
                    thickness={style.timeCurrentThickness}/>
            }
        </>
    )
}