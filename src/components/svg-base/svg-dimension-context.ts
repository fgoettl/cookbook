import {createContext} from "react";
import {RectangleDimensions} from "../../models/geometry2d/rectangle-dimensions.ts";

export const SvgDimensionContext = createContext<RectangleDimensions | undefined>(undefined);