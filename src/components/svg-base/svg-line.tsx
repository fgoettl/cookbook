import {LineStyle} from "../../models/svg-styles/line-style.ts";
import {Line} from "../../models/geometry2d/line.ts";
import {useContext} from "react";
import {SvgDimensionContext} from "./svg-dimension-context.ts";
import {Circle} from "../../models/geometry2d/circle.ts";
import {circleLineIntersection} from "../../utils/geometry2d/intersections.ts";
import {SvgLineSegment} from "./svg-line-segment.tsx";

interface SvgLineProps extends LineStyle {
    line: Line,
}

export function SvgLine({line, ...style}: SvgLineProps) {
    const {center, width, height} = useContext(SvgDimensionContext)!;
    const sufficientlyLargeCircle: Circle = {center: center, radius: (width + height) / 2 + (style.thickness ?? 1)};
    const distantLinePoints = circleLineIntersection(line, sufficientlyLargeCircle);
    if (distantLinePoints.length < 2) return null;
    return <SvgLineSegment lineSegment={{start: distantLinePoints[0], end: distantLinePoints[1]}} {...style}/>
}