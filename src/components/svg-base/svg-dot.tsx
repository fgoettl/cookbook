import {Vec2d} from "../../models/geometry2d/vec2d.ts";

interface SvgDotProps {
    p: Vec2d,
    size?: number,
    color?: string,
}

export function SvgDot({p, size=2, color="black"}: SvgDotProps){
    return (
        <circle cx={p.x} cy={p.y} r={size} fill={color}/>
    )
}