import {PropsWithChildren, useState} from "react";
import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {TextStyle} from "../../models/svg-styles/text-style.ts";
import {SvgMeasure} from "./svg-measure.tsx";

interface SvgTextProps extends TextStyle, PropsWithChildren {
    anchor?: Vec2d,
    x?: number,
    y?: number,
    maxWidth?: number,
}

export function SvgText({
                            anchor = {x: 0, y: 0},
                            x,
                            y,
                            textAnchor = "middle",
                            dominantBaseline = "central",
                            maxWidth,
                            color = "#000",
                            fontFamily = "inherit",
                            fontSize = 14,
                            fontWeight = "normal",
                            fontStyle = "normal",
                            children,
                        }: SvgTextProps) {
    const [factor, setFactor] = useState<number>(1);
    return (
        <g transform={`translate(${x ?? anchor.x} ${y ?? anchor.y}) scale(${factor} -1)`}>
            <SvgMeasure setDimensions={(dimensions) => {
                if (maxWidth !== undefined && dimensions.width > maxWidth) setFactor(maxWidth / dimensions.width);
            }}>
                <text x={0}
                      y={0}
                      textAnchor={textAnchor}
                      dominantBaseline={dominantBaseline}
                      fill={color}
                      fontFamily={fontFamily}
                      fontSize={fontSize}
                      fontWeight={fontWeight}
                      fontStyle={fontStyle}>
                    {children}
                </text>
            </SvgMeasure>
        </g>
    )
}