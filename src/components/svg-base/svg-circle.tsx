import {Circle} from "../../models/geometry2d/circle.ts";

interface SvgCircleProps {
    circle: Circle,
    color?: string,
    thickness?: number,
}

export function SvgCircle({circle, color = "black", thickness = 1}: SvgCircleProps) {
    return <circle cx={circle.center.x}
                   cy={circle.center.y}
                   r={circle.radius}
                   stroke={color}
                   fill="none"
                   strokeWidth={thickness}/>
}