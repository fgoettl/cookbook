import {Circle} from "../../models/geometry2d/circle.ts";

interface SvgDiscProps {
    circle: Circle,
    color?: string,
}

export function SvgDisc({circle, color = "black"}: SvgDiscProps) {
    return <circle cx={circle.center.x}
                   cy={circle.center.y}
                   r={circle.radius}
                   stroke={color}
                   fill={color}/>
}