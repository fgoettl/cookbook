import {PropsWithChildren, useState} from "react";
import {RectangleDimensions} from "../../models/geometry2d/rectangle-dimensions.ts";
import {completeRectangleDimensions} from "../../utils/geometry2d/rectangle.ts";

interface SvgMeasureProps extends PropsWithChildren {
    setDimensions: (dimensions: RectangleDimensions) => void,
    onlyOnce?: boolean,
}

export function SvgMeasure({setDimensions, onlyOnce = false, children}: SvgMeasureProps) {
    const [measured, setMeasured] = useState<boolean>(false);
    return (
        <g ref={(element) => {
            if (element !== null && !measured) {
                const boundingBox = element.getBBox();
                setDimensions(completeRectangleDimensions({
                    x: boundingBox.x,
                    y: boundingBox.y,
                    width: boundingBox.width,
                    height: boundingBox.height,
                })!);
                if (onlyOnce)
                    setMeasured(true);
            }
        }}>
            {children}
        </g>
    )
}