import {LineSegment} from "../../models/geometry2d/line-segment.ts";
import {LineStyle} from "../../models/svg-styles/line-style.ts";

interface SvgLineSegmentProps extends LineStyle {
    lineSegment: LineSegment,
}

export function SvgLineSegment({
                                   lineSegment,
                                   thickness = 1,
                                   color = "black",
                                   strokeDasharray = [],
                                   strokeLinecap = "butt",
                               }: SvgLineSegmentProps) {
    const strokeDasharrayString = strokeDasharray.length === 0 ? undefined :
                                  strokeDasharray.map((x) => String(x)).join(" ");
    return (
        <line x1={lineSegment.start.x}
              y1={lineSegment.start.y}
              x2={lineSegment.end.x}
              y2={lineSegment.end.y}
              stroke={color}
              strokeWidth={thickness}
              strokeDasharray={strokeDasharrayString}
              strokeLinecap={strokeLinecap}/>
    )
}