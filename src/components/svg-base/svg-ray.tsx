import {LineStyle} from "../../models/svg-styles/line-style.ts";
import {Ray} from "../../models/geometry2d/ray.ts";
import {useContext} from "react";
import {SvgDimensionContext} from "./svg-dimension-context.ts";
import {Circle} from "../../models/geometry2d/circle.ts";
import {circleLineIntersection} from "../../utils/geometry2d/intersections.ts";
import {rayToLine} from "../../utils/geometry2d/lines.ts";
import {polarToCartesian} from "../../utils/geometry2d/polar-coordinates.ts";
import {sp} from "../../utils/geometry2d/vector-operations.ts";
import {SvgLineSegment} from "./svg-line-segment.tsx";

interface SvgRayProps extends LineStyle {
    ray: Ray,
}

export function SvgRay({ray, ...style}: SvgRayProps) {
    const {center, width, height} = useContext(SvgDimensionContext)!;
    const sufficientlyLargeCircle: Circle = {center: center, radius: (width + height) / 2 + (style.thickness ?? 1)};
    const distantLinePoints = circleLineIntersection(rayToLine(ray), sufficientlyLargeCircle);
    if (distantLinePoints.length < 2) return null;
    const parallelUnit = polarToCartesian({r: 1, angle: ray.angle});
    const rayDistantPoint = sp(distantLinePoints[0], parallelUnit) > sp(distantLinePoints[1], parallelUnit) ? distantLinePoints[0] : distantLinePoints[1];
    return <SvgLineSegment lineSegment={{start: ray.start, end: rayDistantPoint}} {...style}/>;
}