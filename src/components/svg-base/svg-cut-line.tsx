import {LineStyle} from "../../models/svg-styles/line-style.ts";
import {Vec2d} from "../../models/geometry2d/vec2d.ts";
import {polarToCartesian} from "../../utils/geometry2d/polar-coordinates.ts";
import {SvgLineSegment} from "./svg-line-segment.tsx";
import {add, mulSV} from "../../utils/geometry2d/vector-operations.ts";
import {SvgRay} from "./svg-ray.tsx";
import {SvgLine} from "./svg-line.tsx";

interface SvgCutLineProps extends LineStyle {
    anchor: Vec2d,
    angle: number,
    min?: number,
    max?: number,
}

export function SvgCutLine({anchor, angle, min, max, ...style}: SvgCutLineProps) {
    const unit = polarToCartesian({r: 1, angle: angle});
    return (
        min !== undefined ? (
                              max !== undefined ? <SvgLineSegment
                                                    lineSegment={{start: add(anchor, mulSV(min, unit)), end: add(anchor, mulSV(max, unit))}} {...style}/> :
                              <SvgRay ray={{start: add(anchor, mulSV(min, unit)), angle: angle}} {...style}/>
                          ) :
        max !== undefined ? <SvgRay ray={{start: add(anchor, mulSV(max, unit)), angle: angle + Math.PI}} {...style}/> :
        <SvgLine line={{anchor: anchor, angle: angle}} {...style}/>
    )
}