import {PropsWithChildren} from "react";
import {IncompleteRectangleDimensions} from "../../models/geometry2d/rectangle-dimensions.ts";
import {completeRectangleDimensions} from "../../utils/geometry2d/rectangle.ts";
import {SvgDimensionContext} from "./svg-dimension-context.ts";

interface SvgProps extends PropsWithChildren {
    width: number,
    height: number,
    viewBox?: IncompleteRectangleDimensions,
    backgroundColor?: string,
}

export function Svg({width, height, viewBox, backgroundColor, children}: SvgProps) {
    const completedViewBox = completeRectangleDimensions(viewBox ?? {x: 0, y: 0, width: width, height: height});
    if (completedViewBox === undefined) {
        console.error("SVG ERROR (Svg): Insufficient viewBox provided. Either provide no viewBox or complete viewBox information.")
        return null;
    }

    const internalViewBoxLeft = completedViewBox.x;
    const internalViewBoxBottom = -completedViewBox.y;
    const internalViewBoxWidth = completedViewBox.width;
    const internalViewBoxHeight = completedViewBox.height;

    return (
        <SvgDimensionContext.Provider value={completedViewBox}>
            <svg width={width}
                 height={height}
                 viewBox={`${internalViewBoxLeft} ${internalViewBoxBottom} ${internalViewBoxWidth} ${internalViewBoxHeight}`}>
                {backgroundColor && <rect x={internalViewBoxLeft}
                                          y={internalViewBoxBottom}
                                          width={internalViewBoxWidth}
                                          height={internalViewBoxHeight}
                                          fill={backgroundColor}/>}
                <g transform={`scale(1 -1) translate(0 ${-internalViewBoxHeight})`}>
                    {children}
                </g>
            </svg>
        </SvgDimensionContext.Provider>
    )
}