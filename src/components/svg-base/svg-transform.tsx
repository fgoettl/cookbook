import {PropsWithChildren} from "react";
import {Affine2d} from "../../models/geometry2d/affine2d.ts";
import {Mat2d} from "../../models/geometry2d/mat2d.ts";
import {IDENTITY} from "../../utils/geometry2d/matrices.ts";

interface SvgTransformProps extends PropsWithChildren {
    affine?: Affine2d,
    matrix?: Mat2d,
}

export function SvgTransform({affine, matrix, children}: SvgTransformProps) {
    const mat = affine?.mat ?? matrix ?? IDENTITY;
    const offset = affine?.offset ?? {x: 0, y: 0};
    return (
        <g transform={`matrix(${mat.xx} ${mat.yx} ${mat.xy} ${mat.yy} ${offset.x} ${offset.y})`}>
            {children}
        </g>
    )
}