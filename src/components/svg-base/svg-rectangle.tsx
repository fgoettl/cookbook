import {completeRectangleDimensions} from "../../utils/geometry2d/rectangle.ts";
import {IncompleteRectangleDimensions} from "../../models/geometry2d/rectangle-dimensions.ts";

interface SvgRectangleProps {
    dimensions: IncompleteRectangleDimensions,

    fillColor?: string,
    borderColor?: string,
    borderThickness?: number,
    r?: number,
    rx?: number,
    ry?: number,
}

export function SvgRectangle({
                                 dimensions,
                                 fillColor,
                                 borderColor,
                                 borderThickness,
                                 r,
                                 rx,
                                 ry,
                             }: SvgRectangleProps) {
    const completedDimensions = completeRectangleDimensions(dimensions);

    if (completedDimensions === undefined) {
        console.error("SVG ERROR (SvgRectangle): Insufficient dimensions. Specify enough dimension properties to unambiguously define the rectangle.");
        return null;
    }

    return (
        <rect x={completedDimensions.x}
              y={completedDimensions.y}
              width={completedDimensions.width}
              height={completedDimensions.height}
              fill={fillColor}
              stroke={borderColor}
              strokeWidth={borderThickness}
              rx={rx ?? r}
              ry={ry ?? r}/>
    )
}