import {Polygon} from "../../models/geometry2d/polygon.ts";
import {SvgDot} from "./svg-dot.tsx";
import {SvgLineSegment} from "./svg-line-segment.tsx";

interface SvgPolygonProps {
    polygon: Polygon,
    fillColor?: string,
    borderColor?: string,
    borderThickness?: number,
}

export function SvgPolygon({polygon, fillColor = "none", borderColor = "black", borderThickness = 1}: SvgPolygonProps) {
    switch (polygon.points.length) {
        case 0:
            return null;
        case 1:
            return <SvgDot p={polygon.points[0]} color={borderColor ?? fillColor} size={borderThickness ?? 1}/>;
        case 2:
            return <SvgLineSegment lineSegment={{start: polygon.points[0], end: polygon.points[1]}}
                                   color={borderColor ?? fillColor}
                                   thickness={borderThickness ?? 1}/>;
        default:
            return <path d={"M " + polygon.points.map((p) => `${p.x}, ${p.y}`).join(" L ") + " Z"}
                         fill={fillColor}
                         stroke={borderColor}
                         strokeWidth={borderThickness ?? 1}/>;
    }
}