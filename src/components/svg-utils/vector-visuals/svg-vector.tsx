import {Vec2d} from "../../../models/geometry2d/vec2d.ts";
import {LineStyle} from "../../../models/svg-styles/line-style.ts";
import {ArrowHeadStyle} from "../../../models/svg-styles/arrow-head-style.ts";
import {abs, mulSV, normalize, sub} from "../../../utils/geometry2d/vector-operations.ts";
import {DEFAULT_ARROW_HEAD_LENGTH, SvgArrowHead} from "./svg-arrow-head.tsx";
import {SvgLineSegment} from "../../svg-base/svg-line-segment.tsx";
import {angle} from "../../../utils/geometry2d/polar-coordinates.ts";

interface SvgVectorProps {
    v: Vec2d,
    anchor?: Vec2d,
    color?: string,
    lineStyle?: LineStyle,
    arrowHeadStyle?: ArrowHeadStyle,
}

export function SvgVector({
                              v,
                              anchor = {x: 0, y: 0},
                              color,
                              lineStyle = {thickness: 2},
                              arrowHeadStyle = {},
                          }: SvgVectorProps) {
    const relativeV = sub(v, anchor);
    const arrowLength = abs(relativeV);
    const lengthReduction = Math.min(arrowLength, arrowHeadStyle?.length ?? DEFAULT_ARROW_HEAD_LENGTH);
    const endLine = sub(v, mulSV(lengthReduction, normalize(relativeV)));
    return (
        <>
            <SvgLineSegment lineSegment={{start: anchor, end: endLine}}
                            color={color}
                            {...lineStyle}/>
            <SvgArrowHead tip={v} direction={angle(relativeV)} color={color} {...arrowHeadStyle}/>
        </>
    )
}