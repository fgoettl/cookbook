import {Vec2d} from "../../../models/geometry2d/vec2d.ts";
import {ArrowHeadStyle} from "../../../models/svg-styles/arrow-head-style.ts";
import {polarToCartesian} from "../../../utils/geometry2d/polar-coordinates.ts";
import {add, mulSV, normalize, sub, turnPiHalfs} from "../../../utils/geometry2d/vector-operations.ts";
import {SvgPolygon} from "../../svg-base/svg-polygon.tsx";

export const DEFAULT_ARROW_HEAD_LENGTH = 10;

interface SvgArrowHeadProps extends ArrowHeadStyle {
    tip: Vec2d,
    direction?: number,
}

export function SvgArrowHead({
                              tip,
                              direction,
                              color = "black",
                              length = DEFAULT_ARROW_HEAD_LENGTH,
                              width = 12,
                              sag = 6,
                          }: SvgArrowHeadProps) {
    const parallelUnit = direction !== undefined ? polarToCartesian({r: 1, angle: direction}) : normalize(tip);
    const orthogonalUnit = turnPiHalfs(parallelUnit);
    const back = sub(tip, mulSV(length, parallelUnit));
    const left = add(sub(back, mulSV(sag, parallelUnit)), mulSV(width / 2, orthogonalUnit));
    const right = sub(sub(back, mulSV(sag, parallelUnit)), mulSV(width / 2, orthogonalUnit));
    return <SvgPolygon polygon={{points: [tip, right, back, left]}} fillColor={color} borderThickness={0}/>;
}