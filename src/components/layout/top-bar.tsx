import {AppBar, Box, Button, Toolbar, Typography} from "@mui/material";
import {Article, FileDownload, FileUpload} from "@mui/icons-material";
import {Exporter} from "../io/exporter.tsx";
import { useState } from "react";
import { Importer } from "../io/importer.tsx";
import {Latexer} from "../io/latexer.tsx";

export function TopBar() {
    const [exporterOpen, setExporterOpen] = useState(false);
    const [importerOpen, setImporterOpen] = useState(false);
    const [latexerOpen, setLatexerOpen] = useState(false);

    return (
        <AppBar position="fixed" sx={{zIndex: (theme) => theme.zIndex.drawer + 1}}>
            <Toolbar className="gap-2">
                <Typography variant="h6" noWrap component="div">
                    Feastable
                </Typography>
                <Box sx={{flexGrow: 1}}/>
                <Button variant="contained" color="secondary" onClick={() => setLatexerOpen(true)}>
                    <Typography className="pr-2" variant="body2">
                        Generate LaTeX
                    </Typography>
                    <Article/>
                </Button>
                <Latexer open={latexerOpen} onClose={() => setLatexerOpen(false)}/>
                <Button variant="contained" color="secondary" onClick={() => setExporterOpen(true)}>
                    <Typography className="pr-2" variant="body2">
                        Export
                    </Typography>
                    <FileUpload/>
                </Button>
                <Exporter open={exporterOpen} onClose={() => setExporterOpen(false)}/>
                <Button variant="contained" color="secondary"onClick={() => setImporterOpen(true)}>
                    <Typography className="pr-2" variant="body2">
                        Import
                    </Typography>
                    <FileDownload/>
                </Button>
                <Importer open={importerOpen} onClose={() => setImporterOpen(false)}/>
            </Toolbar>
        </AppBar>
    )
}