import {PropsWithChildren} from "react";
import {Box, CssBaseline, Toolbar} from "@mui/material";
import {TopBar} from "./top-bar.tsx";
import {RecipeDrawer} from "./recipe-drawer.tsx";

export function Menus({children}: PropsWithChildren) {
    return (
        <Box sx={{display: 'flex'}}>
            <CssBaseline/>
            <TopBar/>
            <RecipeDrawer/>
            <Box component="main" sx={{flexGrow: 1, p: 3}}>
                <Toolbar/>
                <div className="w-full flex flex-col items-center">
                    {children}
                </div>
            </Box>
        </Box>
    )
}