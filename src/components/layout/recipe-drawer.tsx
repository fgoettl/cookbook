import {useFeastableStore} from "../../store/store.ts";
import {getAllRecipesSelector} from "../../store/recipe-selectors.ts";
import {Box, Button, Drawer, Toolbar, Typography} from "@mui/material";
import {getSelectedRecipeIdSelector, selectRecipeSelector} from "../../store/navigation-selectors.ts";
import AddIcon from "@mui/icons-material/Add";
import {useState} from "react";
import {RecipeCreator} from "../recipe/recipe-creator.tsx";


const DRAWER_WIDTH = 200

export function RecipeDrawer() {
    const [recipeCreatorOpen, setRecipeCreatorOpen] = useState(false);
    const recipes = useFeastableStore(getAllRecipesSelector);
    const selectRecipe = useFeastableStore(selectRecipeSelector);
    const selectedRecipeId = useFeastableStore(getSelectedRecipeIdSelector);
    return (
        <Drawer
            variant="permanent"
            sx={{
                width: DRAWER_WIDTH,
                flexShrink: 0,
                [`& .MuiDrawer-paper`]: {width: DRAWER_WIDTH, boxSizing: "border-box"},
            }}
        >
            <Toolbar/>
            <Box className="my-4 w-full">
                <Button className="w-full"
                        variant="contained"
                        color="secondary"
                        onClick={() => setRecipeCreatorOpen(true)}>
                    <AddIcon/>
                </Button>
            </Box>
            <RecipeCreator
                onClose={() => setRecipeCreatorOpen(false)}
                open={recipeCreatorOpen}/>
            <Box className="flex flex-col overflow-auto gap-2">
                {recipes.map(recipe => {
                    return (
                        <Button key={recipe.id}
                                onClick={() => selectRecipe(recipe)}
                                variant={selectedRecipeId === recipe.id ? "contained" : "text"}>
                            <Typography variant="body1">
                                {recipe.title}
                            </Typography>
                        </Button>
                    );
                })}
            </Box>
        </Drawer>
    )
}