// import {useFeastableStore} from "../../store/store.ts";
// import {
//     editTaskSelector, selectedRecipeSectionsSelector,
//     selectedRecipeTasksSelector,
// } from "../../store/current-recipe-selectors.ts";
// import {GanttTaskInput} from "./gantt-task-input.tsx";
// import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";
// import {cloneDeep} from "lodash";
// import {CreateTask} from "./create-task.tsx";
//
// export function GanttTaskList() {
//     const ganttTasks = useFeastableStore(selectedRecipeTasksSelector);
//     const ganttSections = useFeastableStore(selectedRecipeSectionsSelector);
//     const editTask = useFeastableStore(editTaskSelector);
//
//     function updateTask(task: GanttTask) {
//         return (update: ((draft: GanttTask) => void)) => {
//             const newTask = cloneDeep(task);
//             update(newTask);
//             editTask(newTask);
//         }
//     }
//
//     return (
//         <>
//             {ganttTasks.map((task) => (
//                 <GanttTaskInput key={task.id} task={task} updateTask={updateTask(task)} sections={ganttSections}/>
//             ))}
//             <CreateTask/>
//         </>
//     )
// }