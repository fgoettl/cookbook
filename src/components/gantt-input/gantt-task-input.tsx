import {Flex, Input, Select, Text, Textarea} from "@chakra-ui/react";
import {GanttTask} from "../../models/gantt-diagram/gantt-task.ts";
import {GanttSection} from "../../models/gantt-diagram/gantt-section.ts";

interface GanttTaskInputProps {
    task: GanttTask,
    updateTask: (update:((draft: GanttTask) => void)) => void,
    sections: GanttSection[],
}

export function GanttTaskInput({task, updateTask, sections}: GanttTaskInputProps) {
    return (
        <Flex width="95%"
              height="150px"
              bg="#EEEEEE"
              direction="column">
            <Flex justifyContent="space-between" alignItems="center" flexGrow={0} bg="red">
                <Input type="text"
                       value={task.title}
                       onChange={(e) => updateTask((draft) => {
                           draft.title = e.target.value
                       })}/>
                <Flex gap="20px">

                    <Flex direction="column">
                        <Text>Section:</Text>
                        <Select value={task.sectionId}
                                onChange={(e) => updateTask((draft) => {draft.sectionId = Number(e.target.value)})}>
                            {sections.map((section) => (
                                <option key={section.id} value={section.id}>{section.title ?? section.id}</option>
                            ))}
                        </Select>
                    </Flex>

                    <Flex direction="column">
                        <Text>Duration:</Text>
                        <Input type="number"
                               step="1"
                               min={1}
                               bg="lightgrey"
                               value={task.durationInMinutes}
                               onChange={(e) => updateTask((draft) => {
                                   draft.durationInMinutes = Number(e.target.value);
                               })}/>
                    </Flex>

                    <Flex direction="column">
                        <Text>Dependency:</Text>
                        <Select bg="lightgrey">
                            <option key={-1} value={""}>No predecessor</option>
                        </Select>
                    </Flex>

                    <Flex direction="column">
                        <Text>Plazierungsmodus:</Text>
                        <Select bg="#00F">
                            <option value="1">Item 1</option>
                        </Select>
                    </Flex>


                </Flex>
            </Flex>
            <Textarea bg="lightgrey"
                      flexGrow="1"/>
        </Flex>
    )
}