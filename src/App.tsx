import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import {RecipeDisplay} from "./components/recipe/recipe-display.tsx";
import {CssBaseline, ThemeProvider} from '@mui/material';
import {theme} from './style/theme.ts';
import {Menus} from "./components/layout/menus.tsx";
import {useFeastableStore} from "./store/store.ts";
import {getSelectedRecipeSelector, updateRecipeSelector} from "./store/recipe-selectors.ts";


// TODO:
//  - FEATURES:
//  - Implement second instruction type and automatic calculation of total ingredients (and a setting to activate/deactivate)
//  -
//  - IMPROVEMENTS:
//  - Make recipes deletable and their title editable
//  - Make "global actions" only possible when not in edit mode
//  - Refactor code (e.g. dialogs, download actions, etc.)
//  - Persist store
//  - Allow recipe settings (e.g. color)
//  - Allow style settings (e.g. spaces)
//  - Improve default texts in dialogs
//  - Allow global title of cookbook
//  - Improved Style (e.g. contents) and settings to choose them in Latexer
//  - Setting to activate/deactivate total ingredient list

function App() {
    const selectedRecipe = useFeastableStore(getSelectedRecipeSelector);
    const updateRecipe = useFeastableStore(updateRecipeSelector);

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <Menus>
                {selectedRecipe &&
                    <RecipeDisplay recipe={selectedRecipe}
                                   updateRecipe={updateRecipe(selectedRecipe)}/>
                }
            </Menus>
        </ThemeProvider>
    );
}


export default App
