import { createTheme } from "@mui/material";

export const theme = createTheme({
    palette: {
        primary: {
            main: '#e65100',
        },
        secondary: {
            main: '#00838f',
        },
    },
});