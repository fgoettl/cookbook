export interface Mat2d {
    //   / xx xy \
    //   \ yx yy /
    xx: number,
    xy: number,
    yx: number,
    yy: number,
}