import {Vec2d} from "./vec2d.ts";

export interface Circle {
    center: Vec2d,
    radius: number,
}