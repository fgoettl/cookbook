import {Vec2d} from "./vec2d.ts";

export interface Ray{
    start: Vec2d,
    angle: number,
}