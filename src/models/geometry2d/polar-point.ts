export interface PolarPoint{
    r: number,
    angle: number,
}