import {Vec2d} from "./vec2d.ts";

export interface Line{
    anchor: Vec2d,
    angle: number,
}