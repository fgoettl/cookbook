import {Vec2d} from "./vec2d.ts";

export interface LineSegment{
    start: Vec2d,
    end: Vec2d,
}