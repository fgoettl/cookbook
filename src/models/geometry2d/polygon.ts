import {Vec2d} from "./vec2d.ts";

export interface Polygon{
    points: Vec2d[],
}