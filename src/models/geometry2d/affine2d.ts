import {Mat2d} from "./mat2d.ts";
import {Vec2d} from "./vec2d.ts";

export interface Affine2d {
    mat: Mat2d,
    offset: Vec2d,
}