export interface GanttStyle {
    timeLinesProtrusionTop: number,
    timeLinesProtrusionBottom: number,
    timeLinesSpacing: (duration: number) => number,
    timeLineThickness: number,
    timeLineColor: string,
    timeStampBottomProtrusion: number,
    timeStampColor: string,
    timeCurrentColor: string,
    timeCurrentThickness: number,

    sectionColorGenerator: (index: number, count: number) => string,
    sectionHeadingLeftPadding: number,
    sectionHeadingRightPadding: number,
    sectionHeadingColor: string,

    taskHeight: number,
    taskBackgroundColor: string,
    taskBorderColor: string,
    taskBorderThickness: number,
    taskBorderRadius: number,
    taskBoxYMargin: number,
    taskBoxXMargin: number,
    taskTextColor: string,

    bottomPadding: number,
}