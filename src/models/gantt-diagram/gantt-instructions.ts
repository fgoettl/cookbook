import {GanttSection} from "./gantt-section.ts";
import {GanttTask} from "./gantt-task.ts";

export interface GanttInstructions {
    sections: GanttSection[],
    tasks: GanttTask[],
}