export interface GanttTask {
    id: number,
    sectionId: number,
    start: number,
    title: string,
    durationInMinutes: number,
}