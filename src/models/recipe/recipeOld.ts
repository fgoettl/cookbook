import {GanttInstructions} from "../gantt-diagram/gantt-instructions.ts";

export interface RecipeOld {
    id: number,
    title: string,
    ganttInstructions: GanttInstructions,
}