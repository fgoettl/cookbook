export interface TextStyle {
    fontFamily?: string,
    fontSize?: number,
    fontWeight?: number | string,
    fontStyle?: "normal" | "italic" | "oblique",
    color?: string,
    textAnchor?: "start" | "middle" | "end",
    dominantBaseline?: "auto" | "text-bottom" | "alphabetic" | "ideographic" | "middle" | "central" | "mathematical" | "hanging" | "text-top",
}