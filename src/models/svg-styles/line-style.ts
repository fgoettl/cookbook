export interface LineStyle {
    color?: string,
    thickness?: number,
    strokeDasharray?: number[],
    strokeLinecap?: "butt" | "round" | "square",
}