export interface ArrowHeadStyle {
    color?: string,
    length?: number,
    width?: number,
    sag?: number,
}