import {FC} from "react";
import {Updater} from "use-immer";

export type InstructionShowProps<DataType> = {
    mode: "Show",
    data: DataType,
}

export type InstructionEditProps<DraftType extends object> = {
    mode: "Edit",
    draft: DraftType,
    updateDraft: Updater<DraftType>,
}

export type InstructionDisplayProps<DataType, DraftType extends object> =
    InstructionShowProps<DataType>
    | InstructionEditProps<DraftType>;

export type InstructionSpecs<DataType, DraftType extends object> = {
    id: number,
    title: string,
    description: string,
    defaultData: DataType,
    toLatex: (instruction: DataType) => string,
    display: FC<InstructionDisplayProps<DataType, DraftType>>
    draftSavable: (draft: DraftType) => boolean,
    finalized: (draft: DraftType) => DataType
    madeDraft: (data: DataType) => DraftType,
}

export type Instruction<DataType, DraftType extends object> = {
    id: number,
    data: DataType,
    specs: InstructionSpecs<DataType, DraftType>,
}




