import {Instruction} from "./instruction.ts";

export type Recipe = {
    id: number,
    title: string,
    instructions: Instruction<unknown, object>[],
}