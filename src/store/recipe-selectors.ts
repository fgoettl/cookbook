import {RecipeSlice} from "./recipe-slice.ts";
import {Recipe} from "../models/recipes/recipe.ts";
import {FeastableStore} from "./store.ts";
import {Updater} from "use-immer";
import {isFunction} from "lodash";
import {produce} from "immer";
import {recipesFromJson, recipesToJson} from "./io/recipe.ts";

export function getAllRecipesSelector(store: RecipeSlice): Recipe[] {
    return store.recipes;
}

export function updateRecipeSelector(store: RecipeSlice): (recipe: Recipe) => Updater<Recipe> {
    return (recipe) =>
        (arg) => {
            if (isFunction(arg)) store.updateRecipe(recipe.id, produce(recipe, arg));
            else store.updateRecipe(recipe.id, arg);
        }
}

// SHARED
export function getSelectedRecipeSelector(store: FeastableStore) {
    return store.selectedRecipeId !== null ? store.getRecipe(store.selectedRecipeId) : undefined;
}

export function createRecipeSelector(store: FeastableStore) {
    return (title: string) => {
        const id = Math.floor(Math.random() * 1000000000);
        const recipe: Recipe = {
            id: id,
            title: title,
            instructions: [],
        };
        store.addRecipe(recipe);
        store.setSelectedRecipeId(id);
    }
}

export function exportStoreSelector(store: FeastableStore) {
    return recipesToJson(store.recipes);
}

export function importStoreSelector(store: FeastableStore) {
    return (json: string) => {
        store.setRecipes(recipesFromJson(json));
    }
}