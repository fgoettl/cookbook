import {Instruction, InstructionSpecs} from "../../models/recipes/instruction.ts";
import {INSTRUCTION_SPECS} from "../../components/instructions/generate-instruction.ts";
import {Recipe} from "../../models/recipes/recipe.ts";

type InstructionDto<DataType> = {
    id: number,
    data: DataType,
    specsId: number,
}

type RecipeDto = {
    id: number,
    title: string,
    instructions: InstructionDto<unknown>[],
}

function instructionToDto<DataType, DraftType extends object>(instruction: Instruction<DataType, DraftType>): InstructionDto<DataType>{
    return {
        id: instruction.id,
        data: instruction.data,
        specsId: instruction.specs.id,
    }
}

function instructionFromDto<DataType, DraftType extends object>(instructionDto: InstructionDto<DataType>): Instruction<DataType, DraftType>{
    return {
        id: instructionDto.id,
        data: instructionDto.data,
        specs: INSTRUCTION_SPECS.find(spec => spec.id === instructionDto.specsId) as unknown as InstructionSpecs<DataType, DraftType>,
    }
}

function recipeToDto(recipe: Recipe): RecipeDto{
    return {
        id: recipe.id,
        title: recipe.title,
        instructions: recipe.instructions.map(instructionToDto),
    }
}

function recipeFromDto(recipeDto: RecipeDto): Recipe {
    return {
        id: recipeDto.id,
        title: recipeDto.title,
        instructions: recipeDto.instructions.map(instructionFromDto),
    }
}

export function recipesToJson(recipes: Recipe[]): string {
    return JSON.stringify(recipes.map(recipeToDto));
}

export function recipesFromJson(json: string): Recipe[] {
    return (JSON.parse(json) as RecipeDto[]).map(recipeFromDto);
}