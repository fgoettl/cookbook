import {StateCreator} from 'zustand';
import {Recipe} from "../models/recipes/recipe.ts";
import {FeastableStore} from "./store.ts";

export interface RecipeSlice {
    recipes: Recipe[];
    addRecipe: (recipe: Recipe) => void;
    setRecipes: (recipes: Recipe[]) => void;
    removeRecipe: (id: number) => void;
    updateRecipe: (id: number, recipe: Recipe) => void;
    getRecipe: (id: number) => Recipe | undefined;
}

export const createRecipeSlice: StateCreator<FeastableStore, [], [], RecipeSlice> = ((set, get) => ({
    recipes: [],
    addRecipe: (recipe) => set(state => ({recipes: [...state.recipes, recipe]})),
    setRecipes: (recipes) => set({recipes: recipes}),
    removeRecipe: (id) => set(state => ({recipes: state.recipes.filter(recipe => recipe.id !== id)})),
    updateRecipe: (id, updatedRecipe) => set(state => ({
        recipes: state.recipes.map(recipe =>
            recipe.id === id ? updatedRecipe : recipe
        )
    })),
    getRecipe: (id) => {
        const {recipes} = get();
        return recipes.find(recipe => recipe.id === id);
    }
}));