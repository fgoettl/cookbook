// import {StateCreator} from "zustand";
// import {FeastableStore} from "./store.ts";
// import {RecipeOld} from "../models/recipe/recipeOld.ts";
// import {produce} from "immer";
// import {GanttTask} from "../models/gantt-diagram/gantt-task.ts";
//
// export interface RecipeSliceOld {
//     recipes: RecipeOld[],
//     selectedRecipeId?: number,
//
//     setSelectedRecipeId: (id: number) => void,
//     editRecipe: (recipe: RecipeOld) => void,
//     editTask: (task: GanttTask) => void,
//     addTask: (task: GanttTask) => void,
// }
//
// export const createRecipeSliceOld: StateCreator<FeastableStore, [], [], RecipeSliceOld> = (set) => ({
//     recipes: [{
//         title: "Wirsingrouladen",
//         ganttInstructions: {
//             tasks: [],
//             sections: [{title: "TestSection", id: 0},{title: "Preparation", id: 1},{title: "Feastability check", id: 2}],
//         },
//         id: 0,
//     }],
//     selectedRecipeId: 0,
//
//     setSelectedRecipeId: (id: number) => {
//         set(() => ({
//             selectedRecipeId: id,
//         }))
//     },
//     editRecipe: (recipe: RecipeOld) => {
//         set(
//             produce((draft: RecipeSliceOld) => {
//                 const recipeToEdit = draft.recipes.find(r => r.id === recipe.id);
//                 if(recipeToEdit === undefined) {
//                     throw new Error("Recipe can not be edited, because the id is not present in the store.");
//                 }
//                 Object.assign(recipeToEdit, recipe);
//             })
//         )
//     },
//     editTask: (task: GanttTask) => {
//         set(
//             produce((draft: RecipeSliceOld) => {
//                 const selectedRecipe = draft.recipes.find(r => r.id === draft.selectedRecipeId);
//                 if(selectedRecipe === undefined) {
//                     throw new Error("Task can not be edited, because the selected recipe id is not present in the store.");
//                 }
//                 const taskToEdit = selectedRecipe.ganttInstructions.tasks.find(t => t.id === task.id);
//                 if(taskToEdit === undefined) {
//                     throw new Error("Task can not be edited, because the task id is not present in the store.");
//                 }
//                 Object.assign(taskToEdit, task);
//             })
//         )
//     },
//     addTask: (task: GanttTask) => {
//         set(
//             produce((draft: RecipeSliceOld) => {
//                 const selectedRecipe = draft.recipes.find(r => r.id === draft.selectedRecipeId);
//                 if(selectedRecipe === undefined) {
//                     throw new Error("Task can not be added, because the selected recipe id is not present in the store.");
//                 }
//                 selectedRecipe.ganttInstructions.tasks.push(task);
//             })
//         )
//     }
// })