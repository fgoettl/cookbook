import {StateCreator} from 'zustand';
import {FeastableStore} from "./store.ts";

export interface NavigationSlice {
    selectedRecipeId: number | null,

    setSelectedRecipeId: (id: number | null) => void,
}

export const createNavigationSlice: StateCreator<FeastableStore, [], [], NavigationSlice> = ((set) => ({
    selectedRecipeId: null,
    setSelectedRecipeId: (id: number | null) => {
        set({selectedRecipeId: id});
    }
}));