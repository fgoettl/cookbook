// import {RecipeSliceOld} from "./recipe-slice-old.ts";
//
// export function selectedRecipeSelector(store: RecipeSliceOld) {
//     return store.recipes.find((r) => r.id === store.selectedRecipeId);
// }
//
// export function selectedRecipeTasksSelector(store: RecipeSliceOld) {
//     return selectedRecipeSelector(store)?.ganttInstructions.tasks ?? [];
// }
//
// export function selectedRecipeSectionsSelector(store: RecipeSliceOld) {
//     return selectedRecipeSelector(store)?.ganttInstructions.sections ?? [];
// }
//
// export function selectedRecipeGanttInstructinsSelector(store: RecipeSliceOld) {
//     return selectedRecipeSelector(store)?.ganttInstructions ?? {sections:[], tasks:[]};
// }
//
// export function editRecipeSelector(store: RecipeSliceOld) {
//     return store.editRecipe;
// }
//
// export function editTaskSelector(store: RecipeSliceOld) {
//     return store.editTask;
// }
//
// export function addTaskSelector(store: RecipeSliceOld) {
//     return store.addTask;
// }
//
// export function addDefaultTaskSelector(store: RecipeSliceOld) {
//     return () => {
//         const selectedRecipe = selectedRecipeSelector(store);
//         if (selectedRecipe === undefined) return;
//         const freeId = selectedRecipe.ganttInstructions.tasks.map((t) => t.id).reduce((a, b) => Math.max(a, b), 0) + 1;
//         addTaskSelector(store)({
//             id: freeId,
//             sectionId: 0,
//             start: 0,
//             title: "",
//             durationInMinutes: 1,
//         });
//     }
// }