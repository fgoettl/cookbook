import {create} from "zustand";
import {RecipeSlice, createRecipeSlice} from "./recipe-slice.ts";
import {createNavigationSlice, NavigationSlice} from "./navigation-slice.ts";

export type FeastableStore = RecipeSlice & NavigationSlice;

export const useFeastableStore = create<FeastableStore>()(
    (...a) => ({
        ...createRecipeSlice(...a),
        ...createNavigationSlice(...a),
    })
)