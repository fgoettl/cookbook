import {NavigationSlice} from "./navigation-slice.ts";
import {Recipe} from "../models/recipes/recipe.ts";

export function getSelectedRecipeIdSelector(store: NavigationSlice) {
    return store.selectedRecipeId;
}

export function setSelectedRecipeIdSelector(store: NavigationSlice) {
    return store.setSelectedRecipeId;
}

export function selectRecipeSelector(store: NavigationSlice) {
    return (recipe: Recipe) => {store.setSelectedRecipeId(recipe.id)};
}