// import {BaseDirectory, readTextFile} from '@tauri-apps/api/fs';
// import {GanttInstructions} from "../models/gantt-diagram/gantt-instructions.ts";
// import {GanttTask} from "../models/gantt-diagram/gantt-task.ts";
// import {GanttSection} from "../models/gantt-diagram/gantt-section.ts";
//
// //https://www.typescriptlang.org/docs/handbook/functions.html
// async function readRecipe(recipeName: string): GanttInstructions[]{
//     try {
//         const contents = await readTextFile(recipeName, { dir: BaseDirectory });
//         let gantTasks = JSON.parse(contents) as GanttTask[];
//         let sections: GanttSection[] = [];
//         sections.forEach((task) => {
//             if(!sections.includes(task.id)){
//                 sections.push(task.id);
//             }
//         });
//         return new GanttInstruction(gantTasks, sections);
//     } catch (error) {
//         console.error('Error reading gantt tasks:', error);
//         return [];
//     }
// }
//
